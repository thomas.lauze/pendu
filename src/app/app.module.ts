import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { JoueurComponent } from './joueur/joueur.component';
import { PenduComponent } from './pendu/pendu.component';


@NgModule({
  declarations: [
    AppComponent,
    JoueurComponent,
    PenduComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
